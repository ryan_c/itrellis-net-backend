﻿using EcommBackend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommBackend.Repositories
{
    public interface IProductRepository
    {
        Product GetProduct(int productId);
        IEnumerable<Product> GetAllProducts();
    }
}