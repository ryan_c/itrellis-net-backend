﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Dapper;
using EcommBackend.Models;

namespace EcommBackend.Repositories
{
    public class ProductRepository : BaseSqLiteRepository, IProductRepository
    {
        public IEnumerable<Product> GetAllProducts()
        {
            if (!File.Exists(DatabaseFile)) return null;

            using (var conn = DatabaseConnection())
            {
                conn.Open();
                List<Product> result = conn.Query<Product>(
                    @"SELECT
                        productId,
                        productName,
                        inventoryQuantity,
                        shipOnWeekends,
                        maxBusinessDaysToShip
                      FROM
                        products").ToList();

                return result;
            }
        }

        public Product GetProduct(int productId)
        {
            if (!File.Exists(DatabaseFile)) return null;

            using (var conn = DatabaseConnection())
            {
                conn.Open();
                Product result = conn.Query<Product>(
                    @"SELECT
                        productId,
                        productName,
                        inventoryQuantity,
                        shipOnWeekends,
                        maxBusinessDaysToShip
                      FROM
                        products
                      WHERE
                        productId = @productId", new { productId }).FirstOrDefault();

                return result;
            }
        }
    }
}