﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Web;

namespace EcommBackend.Repositories
{
    public class BaseSqLiteRepository
    {
        public static string DatabaseFile
        {
            get { return AppContext.BaseDirectory + "\\App_Data\\Products.sqlite"; }
        }

        public static SQLiteConnection DatabaseConnection()
        {
            return new SQLiteConnection("Data Source=" + DatabaseFile);
        }
    }
}