﻿using EcommBackend.Models;
using EcommBackend.Repositories;
using EcommBackend.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommBackend.Controllers
{
    [RoutePrefix("shipping")]
    public class ShippingController : ApiController
    {
        private IProductRepository _productRepository;

        public ShippingController()
        {
            _productRepository = new ProductRepository();
        }

        [HttpPost]
        [Route("calculator")]
        public IHttpActionResult CalculateShipping(ShippingCalculatorRequest request)
        {
            Product product = _productRepository.GetProduct(request.productId);

            if (product != null)
            {
                var daysToAdd = WorkingDayCalculatorUtilities.GetWorkingDays(request.orderedOn, product.maxBusinessDaysToShip, product.shipOnWeekends);
                var expectedShipDate = request.orderedOn.AddDays(daysToAdd);

                return Ok(new ShippingCalculatorResponse()
                {
                    productId = request.productId,
                    expectedShipDate = expectedShipDate
                });
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
