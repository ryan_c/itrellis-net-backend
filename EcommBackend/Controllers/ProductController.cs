﻿using EcommBackend.Models;
using EcommBackend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace EcommBackend.Controllers
{
    [RoutePrefix("products")]
    public class ProductController : ApiController
    {
        private ProductRepository _productRepository;

        public ProductController()
        {
            _productRepository = new ProductRepository();
        }

        [HttpGet]
        [Route("product")]
        public IHttpActionResult GetProduct(int productId)
        {
            Product result = _productRepository.GetProduct(productId);

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [Route("products")]
        public IHttpActionResult GetAllProducts()
        {
            List<Product> results = _productRepository.GetAllProducts().ToList();
            
            if (results != null)
            {
                return Ok(results);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
