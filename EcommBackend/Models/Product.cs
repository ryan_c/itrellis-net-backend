﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommBackend.Models
{
    public class Product
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public int inventoryQuantity { get; set; }
        public bool shipOnWeekends { get; set; }
        public int maxBusinessDaysToShip { get; set; }
    }
}