﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommBackend.Models
{
    public class ShippingCalculatorRequest
    {
        public int productId { get; set; }
        public DateTime orderedOn { get; set; }
    }
}