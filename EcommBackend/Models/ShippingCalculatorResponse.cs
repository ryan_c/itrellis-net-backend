﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommBackend.Models
{
    public class ShippingCalculatorResponse
    {
        public int productId { get; set; }
        public DateTime expectedShipDate { get; set; }
    }
}