﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EcommBackend.Utilities
{
    public static class WorkingDayCalculatorUtilities
    {
        public static int GetWorkingDays(DateTime from, int businessDays, bool keepWeekends)
        {
            var workingDays = 0;

            int day = 1;
            while(day < businessDays)
            {
                if (!keepWeekends)
                {
                    workingDays++;

                    if (from.AddDays(day).DayOfWeek == DayOfWeek.Saturday || from.AddDays(day).DayOfWeek == DayOfWeek.Sunday)
                    {
                        businessDays++;
                    }
                }
                else
                {
                    workingDays++;
                }

                day++;
            }

            return workingDays;
        }

    }
}