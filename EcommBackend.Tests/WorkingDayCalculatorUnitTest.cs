﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EcommBackend.Tests
{
    [TestClass]
    public class WorkingDayCalculatorUnitTest
    {
        [TestMethod]
        public void TestWorkingDays()
        {
            var datefrom = new DateTime(2018, 06, 11);
            var businessDays = 13;

            var expectedWithoutWeekends = 17;
            var expectedWithWeekends = 13;

            Assert.AreEqual(expectedWithoutWeekends, EcommBackend.Utilities.WorkingDayCalculatorUtilities.GetWorkingDays(datefrom, businessDays, false));
            Assert.AreEqual(expectedWithWeekends, EcommBackend.Utilities.WorkingDayCalculatorUtilities.GetWorkingDays(datefrom, businessDays, true));   
        }
    }
}
