# iTrellis .NET Backend

This project consists of a simple WebAPI project that serves products and calculates the shipping dates according to the coding challenge.

## Running

This is a standard .NET solution with ASP.NET running Web API 2.0. Please allow or make sure to restore the NuGet packages. This project uses Dapper and a SqLite database as the backend data store for portability. The SqLite database is stored in the repository already for ease of running. It contains a table containing ony the sample data (no additional data.)

## Note

When running this solution, make sure that you can also run the frontend and have it able to access the network address / port that is is running from.
